//Navbar smooth scrolling javascript
$('.navbar a').on('click', function(e) {
    if(this.hash !== '') {
        e.preventDefault();

        const hash = this.hash;

        $('html, body').animate(
            {
                scrollTop: $(hash).offset().top
            },
            800
        );
    }
});

// //navbar scrollspy
// $('body').scrollspy({ target: '#navbar-fixed-top' })

//navbar active yellow
$(".navbar-light li").on("click", function() {
    $(".navbar-light li").removeClass("active");
    $(this).addClass("active");
});

//setting display carousel responsive
$('.owl-carousel').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout:5000,
    margin: 80,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:2
        }
    }
});